package com.practice.problems;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;

import static com.practice.problems.LeetCode.*;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LeetCodeTests {
    @Ignore
    @Test
    public void testRotateKNormalCase() {
        int[] nums = {1,2,3,4,5,6,7};
        int k = 3;
        rotate(nums, k);
        assertArrayEquals(new int[]{5,6,7,1,2,3,4}, nums);
    }

    @Ignore
    @Test
    public void testMoveZeroesAllZeroes() {
        int[] nums = {0, 0, 0, 0};
        moveZeroes(nums);
        assertArrayEquals(nums, nums);
    }

    @Ignore
    @Test
    public void testMoveZeroesAllZeroesAtBeginning() {
        int[] nums = {0, 0, 0, 0, 5, 6, 7};
        moveZeroes(nums);
        assertArrayEquals(new int[]{5,6,7,0,0,0,0}, nums);
    }

    @Ignore
    @Test
    public void testMoveZeroesAllZeroesAtEnd() {
        int[] nums = {5, 6, 7, 0, 0, 0, 0};
        moveZeroes(nums);
        assertArrayEquals(new int[]{5,6,7,0,0,0,0}, nums);
    }

    @Ignore
    @Test
    public void testMoveZeroesNoZeroes() {
        int[] nums = {5, 6, 7};
        moveZeroes(nums);
        assertArrayEquals(new int[]{5,6,7}, nums);
    }

    @Ignore
    @Test
    public void testMoveZeroesNormalCase() {
       int[] nums = {0, 1, 0, 3, 12};
       moveZeroes(nums);
       assertArrayEquals(new int[]{1, 3, 12, 0, 0}, nums);
    }

    @Ignore
    @Test
    public void testRotate4x4Matrix() {
        int[][] matrix = {{ 5, 1, 9, 11},
                { 2, 4, 8, 10},
                {13, 3, 6, 7},
                {15, 14, 12, 16}};
        int[][] matrixExpectedRes = {{ 15, 13, 2,5},
                { 14, 3, 4, 1},
                {12, 6, 8, 9},
                {16, 7, 10, 11}};
        rotate(matrix);
        assertTrue(Arrays.deepEquals(matrix, matrixExpectedRes));
    }

    @Test
    public void testAddTwoNumbersSameLengthWithoutOverflow() {
        ListNode l1 = makeListFromNumber(342);
        ListNode l2 = makeListFromNumber(465);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 807);
    }

    @Test
    public void testAddTwoNumbersLongerNuml1() {
        ListNode l1 = makeListFromNumber(342);
        ListNode l2 = makeListFromNumber(46);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 388);
    }

    @Test
    public void testAddTwoNumbersLongerNuml2() {
        ListNode l1 = makeListFromNumber(46);
        ListNode l2 = makeListFromNumber(342);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 388);
    }

    @Test
    public void testAddTwoNumbersL2Is0() {
        ListNode l1 = makeListFromNumber(46);
        ListNode l2 = makeListFromNumber(0);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 46);
    }

    @Test
    public void testAddTwoNumbersL1Is0() {
        ListNode l1 = makeListFromNumber(0);
        ListNode l2 = makeListFromNumber(46);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 46);
    }

    @Test
    public void testAddTwoNumbersWithFinalOverflow() {
        ListNode l1 = makeListFromNumber(5);
        ListNode l2 = makeListFromNumber(5);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 10);
    }

    @Test
    public void testAddTwoNumbersWithOverflowAtEndOfDigit() {
        ListNode l1 = makeListFromNumber(415);
        ListNode l2 = makeListFromNumber(85);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 500);
    }

    @Test
    public void testAddTwoNumbersFailing0plusLong() {
        ListNode l1 = makeListFromNumber(0);
        ListNode l2 = makeListFromNumber(872);

        ListNode sumAsList = LeetCode.addTwoNumbers(l1, l2);
        int res = makeIntFromListNode(sumAsList);
        assertEquals(res, 872);
    }

    private ListNode makeListFromNumber(int num) {
        ListNode result = new ListNode(0);
        ListNode pointer = result;
        while (num > 0) {
            pointer.val = (num % 10);
            num /= 10;
            if (num > 0)
                pointer.next = new ListNode(0);
            pointer = pointer.next;
        }
        return result;
    }

    private int makeIntFromListNode(ListNode num) {
        int res = 0;
        ListNode pointer = num;
        for (int i = 1; pointer != null; i *= 10) {
            res += pointer.val * i;
            pointer = pointer.next;
        }
        return res;
    }
}
