package com.practice.problems;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class LibraryTest extends TestCase {

    Library library = new Library(
            "TITLE: Hitchhiker's Guide to the Galaxy\n" +
                    "AUTHOR: Douglas Adams\n" +
                    "DESCRIPTION: Seconds before the Earth is demolished to make way for a galactic freeway,\n" +
                    "Arthur Dent is plucked off the planet by his friend Ford Prefect, a researcher for the\n" +
                    "revised edition of The Hitchhiker's Guide to the Galaxy who, for the last fifteen years,\n" +
                    "has been posing as an out-of-work actor.\n" +
                    "\n" +
                    "TITLE: Dune\n" +
                    "AUTHOR: Frank Herbert\n" +
                    "DESCRIPTION: The troubles begin when stewardship of Arrakis is transferred by the\n" +
                    "Emperor from the Harkonnen Noble House to House Atreides. The Harkonnens don't want to\n" +
                    "give up their privilege, though, and through sabotage and treachery they cast young\n" +
                    "Duke Paul Atreides out into the planet's harsh environment to die. There he falls in\n" +
                    "with the Fremen, a tribe of desert dwellers who become the basis of the army with which \n" +
                    "he will reclaim what's rightfully his. Paul Atreides, though, is far more than just a\n" +
                    "usurped duke. He might be the end product of a very long-term genetic experiment\n" +
                    "designed to breed a super human; he might be a messiah. His struggle is at the center\n" +
                    "of a nexus of powerful people and events, and the repercussions will be felt throughout\n" +
                    "the Imperium.\n" +
                    "\n" +
                    "TITLE: A Song Of Ice And Fire Series\n" +
                    "AUTHOR: George R.R. Martin\n" +
                    "DESCRIPTION: As the Seven Kingdoms face a generation-long winter, the noble Stark family\n" +
                    "confronts the poisonous plots of the rival Lannisters, the emergence of the\n" +
                    "White Walkers, the arrival of barbarian hordes, and other threats.\n");

    @Test
    public void testWordAppearSingleTime() {
        List<String> first_results = library.search("Arrakis");
        assertThat(first_results, hasItem(containsString("Dune")));
    }

    @Test
    public void testAnotherWordAppearSingleTime() {
        List<String> first_results = library.search("winter");
        assertThat(first_results, hasItem(containsString("A Song Of Ice And Fire Series")));
    }

    @Test
    public void testAnotherWordAppearSingleTimeAndTitle() {
        List<String> first_results = library.search("demolished");
        assertThat(first_results, hasItem(containsString("Hitchhiker's Guide to the Galaxy")));
    }

    @Test
    public void testMultipleTimesWordInEachTitleWillReturnOnlyOnceEach() {
        List<String> fourth_results = library.search("the");
        assertThat(fourth_results.size(), equalTo(3));
    }

    @Test
    public void testMultipleTimesWordInEachTitleReturnCorrectTitles() {
        List<String> fourth_results = library.search("the");
        assertThat(fourth_results, allOf(
                hasItem(containsString("Dune")),
                hasItem(containsString("A Song Of Ice And Fire Series")),
                hasItem(containsString("Hitchhiker's Guide to the Galaxy"))));
    }

}
