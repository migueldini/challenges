package com.practice.problems;

import java.util.HashMap;
import java.util.Map;

public class LeetCode {
    private static void copyArraySolution(int[] nums, int k) {
        int finalPosition = nums.length - k;
        int[] arrayCopy = new int[nums.length];
        System.arraycopy(nums, 0, arrayCopy, 0, nums.length);
        for (int i = 0; i < arrayCopy.length; i++) {
            nums[i] = arrayCopy[finalPosition++ % nums.length];
        }
    }

    public static void rotate(int[] nums, int k) {
        if (nums.length < k)
            return;
        copyArraySolution(nums, k);
    }

    public static boolean containsDuplicate(int[] nums) {
        Map<Integer, Object> elementsAtArray = new HashMap<>();
        Object appeared = new Object();
        for (int i = 0; i < nums.length; i++) {
            if (elementsAtArray.get(nums[i]) != null) {
                return true;
            } else {
                elementsAtArray.put(nums[i], appeared);
            }
        }
        return false;
    }

    public int singleNumber(int[] nums) {
        Map<Integer, Object> elementsAtArray = new HashMap<>();
        Object repeated = new Object();
        Object single = new Object();
        for (int i = 0; i < nums.length; i++) {
            if (elementsAtArray.get(nums[i]) != null) {
                if (elementsAtArray.get(nums[i]).equals(single)) {
                    elementsAtArray.put(nums[i], repeated);
                }
            } else {
                elementsAtArray.put(nums[i], single);
            }
        }
        for (Map.Entry<Integer, Object> e : elementsAtArray.entrySet()) {
            if (e.getValue().equals(single))
                return e.getKey();
        }
        return -1;
    }

    private static void swap(int[] nums, int index1, int index2) {
        int tempHolder = nums[index1];
        nums[index1] = nums[index2];
        nums[index2] = tempHolder;
    }


    // nums = [0, 1, 0, 3, 12]
    public static void moveZeroes(int[] nums) {
        int availPosition = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[availPosition] = nums[i];
                availPosition++;
            }
        }
        while (availPosition < nums.length) {
            nums[availPosition++] = 0;
        }
    }

    public int[] twoSum(int[] nums, int target) {
        int sum1 = -1, sum2 = -1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return null;
    }


    public static void rotate(int[][] matrix) {
        int length = matrix.length;
        for (int i = 0; i < length / 2; i++)
            for (int j = i; j < length - i - 1; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[length - j - 1][i];
                matrix[length - j - 1][i] = matrix[length - i - 1][length - j - 1];
                matrix[length - i - 1][length - j - 1] = matrix[j][length - i - 1];
                matrix[j][length - i - 1] = tmp;
            }
    }


    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int overflow = 0;
        ListNode result = l1;
        ListNode pointer = l1;

        while (l1 != null && l2 != null) {
            int res = l1.val + l2.val + overflow;
            l1.val = res % 10;
            overflow = res / 10;
            pointer = l1;
            l1 = l1.next;
            l2 = l2.next;
        }

        while (l1 != null) {
            int res = l1.val + overflow;
            l1.val = res % 10;
            overflow = res / 10;
            pointer = l1;
            l1 = l1.next;
        }


        if (l1 == null && l2 != null) {
            l1 = pointer;
        }

        while (l2 != null) {
            int res = l2.val + overflow;
            overflow = res / 10;
            pointer.next = new ListNode(res % 10);
            pointer = pointer.next;
            l2 = l2.next;
        }

        if (overflow > 0)
            pointer.next = new ListNode(overflow);
        return result;

    }

    public static String reverseString(String s) {
        StringBuilder sb = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) {
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    public static String reverseString2(String s) {
        char[] chars = s.toCharArray();
        int length = chars.length;
        int halfIndex = length / 2;
        length--;
        for (int i = 0; i < halfIndex; i++) {
            swapChars(chars, i, length - i);
        }
        return new String(chars);
    }

    private static void swapChars(char[] chars, int index1, int index2) {
        char tempHolder = chars[index1];
        chars[index1] = chars[index2];
        chars[index2] = tempHolder;
    }

    public static int reverse(int x) {
        boolean negative = false;
        if (x < 0) {
            negative = true;
            x *= -1;
        }
        int divider  = 10;
        while (x > 0) {
            int reminder = x % divider;
            x /= divider;
        }
    }

    public static void main(String[] args) {
        int input1 = 123;
        int input2 = -123;
        int input3 = 120;
        System.out.println(input1);
        System.out.println(reverse(input1));
        System.out.println(input2);
        System.out.println(reverse(input2));
        System.out.println(input3);
        System.out.println(reverse(input3));
    }

}
