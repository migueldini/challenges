package com.practice.problems;

import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 *
 * Given a flat file of book metadata, write a com.practice.problems.Library class that parses the   book data and provides an API that lets
 * you search for all books containing a word.
 *
 *  API:
 *
 *  com.practice.problems.Library
 *    - <constructor>(input) -> returns a com.practice.problems.Library object
 *    - search(word) -> returns all books that contain the word anywhere in the title, author, or description fields.
 *    Only matches *whole* words.
 *   E.g. Searching for "My" or "book" would match a book containing "My book", but searching for "My b" or "boo" would
 *   *not* match.
 */

class Library {
    //Key: Words; Value: List of titles matching
    private Map<String, Set<String>> wordLookUpTable = new HashMap<>();

    public Library(String info) {
        String[] books = info.split("\\n\\n");
        for (String book : books) {
            String title = extractTitle(book);
            String[] allWords = getCleansedWords(book);
            updateWordsDictionary(title, allWords);
        }
    }

    private void updateWordsDictionary(String title, String[] wordsToCheck) {
        for (String currentWord : wordsToCheck) {
            if (wordLookUpTable.containsKey(currentWord))
                wordLookUpTable.get(currentWord).add(title);
            else {
                Set<String> titlesTemp = new HashSet<>();
                titlesTemp.add(title);
                wordLookUpTable.put(currentWord, titlesTemp);
            }
        }
    }

    private String[] getCleansedWords(String book) {
        String cleansedString = book.replaceAll("\\n", " ");
        cleansedString = cleansedString.replaceAll("[^a-zA-Z0-9 \\-]", "").toLowerCase();
        return cleansedString.split("\\s+");
    }

    private String extractTitle(String book) {
        int beginOfTitle = book.indexOf("TITLE: ") + 7;
        int endOfTitle = book.indexOf("\n", beginOfTitle);
        return book.substring(beginOfTitle, endOfTitle);
    }

    public List<String> search(String wordToSearch) {
        wordToSearch = wordToSearch.toLowerCase();
        if (wordLookUpTable.containsKey(wordToSearch)) {
            return new ArrayList<>(wordLookUpTable.get(wordToSearch));
        }
        return new ArrayList<>();
    }

    public static void main(String[] args) {

    }
}
