package com.practice.problems;

public class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
    }

    @Override
    public String toString() {
        ListNode pointer = this;

        StringBuffer sb = new StringBuffer();
        while (pointer != null) {
            sb.append(pointer.val);
            if (pointer.next != null)
                sb.append(" -> ");
            pointer = pointer.next;
        }

        return "ListNode{" +
                sb.toString() +
                '}';
    }
}